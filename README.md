# ansible-docker-compose

Deploy one or more docker-compose projects and configure based on .env files.

## Requirements

This role is tested on Ubuntu 18.04 and 20.04 with Ansible 2.9.

## Role Variables

This role assumes the following variable structure, with a Traefik compose
config as example:

```yaml
docker_compose_projects:
  - name: openstackproxy
    repo: https://gitlab.com/naturalis/core/openstack/docker-compose-traefik-openstack
    repo_version: develop # Can be a tag, branch or commit
    env:
      TRAEFIK_VERSION: v2.2.8
      TRAEFIK_LOG_LEVEL: DEBUG
      TRAEFIK_API_ENABLED: "true"
      TRAEFIK_API_INSECURE: "true"
```

Apart from that these generic variables need to be set as well:

```yaml
AWS_ACCESS_KEY_ID: access_key_id_for_route53
AWS_SECRET_ACCESS_KEY: secret_access_key_for_route53
env: environmentname # Defaults to 'development'
service: servicename # Defaults to the name of the compose project
```

## Dependencies

As part of this role Docker and docker-compose will be installed. The
installation of Docker is based on the nickjj/ansible-docker role.

## Example Playbook

In this example the docker-compose role is applied and in a separate task an
extra config file is generated that can be used for consumption in a container.
The containers defined in the docker-compose will get started by a handler
after the template has been generated.

```yaml
---
- name: Deploy reverse proxies
  hosts: reverseproxies
  gather_facts: true
  become: true
  tasks:
    - name: Apply docker-compose role
      include_role:
        name: docker-compose
        apply:
          tags: docker-compose
      tags: always

    - name: Add extra config for Traefik
      template:
        src: traefik-config.toml.j2
        dest: /opt/compose_projects/openstackproxy/config/traefik-config.toml
...
```
